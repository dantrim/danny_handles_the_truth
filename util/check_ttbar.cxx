// EDM
#include "AsgTools/ToolHandle.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"

// Base
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"
#include "AsgTools/AnaToolHandle.h"

// std/stl
#include <iostream>
#include <sstream>
#include <string>
#include <memory>
#include <stdexcept>
using namespace std;


// ROOT
#include "TFile.h"
#include "TH1F.h"


int main(int argc, char** argv)
{
    const char * algo = argv[0];
    if(argc < 3)
    {
        cout << "ERROR expect an input file as input and number of events to process" << endl;
        cout << "Usage: " << algo << " <input-file> <n-events>" << endl;
        return 1;
    }
    string input_name = argv[1];
    int n_events = atoi(argv[2]);

    RETURN_CHECK(algo, xAOD::Init());
    xAOD::TEvent event(xAOD::TEvent::kClassAccess);

    std::unique_ptr<TFile> ifile(TFile::Open(input_name.c_str(), "READ"));
    if(!ifile.get() || ifile->IsZombie())
    {
        throw std::logic_error("ERROR Could not open input file: " + input_name);
    }

    // let's store some histograms
    TFile* rfile = new TFile("check_ttbar.root", "RECREATE");
    TH1F* h_origin = new TH1F("h_origin", ";MC Particle Truth Origin;", 40, 0, 40);
    TH1F* h_type = new TH1F("h_type", ";MC Particle Truth Type;", 40, 0, 40);
    TH1F* h_ttbar_n_lep = new TH1F("h_n_lep", ";Number of leptons from top;", 5, 0, 5);

    float lep_pt_cut = 10.0; // GeV
    float lep_eta_cut = 2.4; //


    // let's roll
    RETURN_CHECK( algo, event.readFrom(ifile.get()) );
    const unsigned long long n_entries_in_file = event.getEntries();
    cout << "Input file has " << n_entries_in_file << " total events";
    if(n_events < 0) n_events = n_entries_in_file;
    cout << ", will read " << n_events << " events" << endl;
    for(unsigned long long entry = 0; entry < (unsigned)n_events; ++entry)
    {
        bool ok = event.getEntry(entry) >= 0;
        if(!ok) throw std::logic_error("ERROR getEntry failed");

        if(entry % 1000 == 0)
        {
            cout << algo << "   Processing event " << entry << endl;
        }
        const xAOD::EventInfo* eventinfo = nullptr;
        const xAOD::ElectronContainer* electrons = nullptr;
        const xAOD::MuonContainer* muons = nullptr;

        RETURN_CHECK( algo, event.retrieve( eventinfo, "EventInfo" ) );
        RETURN_CHECK( algo, event.retrieve( electrons, "Electrons" ) );
        RETURN_CHECK( algo, event.retrieve( muons, "Muons" ) );

        int n_true_lep = 0;

        for(const auto & lep : *electrons)
        {
            if(lep->pt() < lep_pt_cut * 1e-3) continue;
            if(fabs(lep->eta()) > lep_eta_cut) continue;

            // Retrieve MCTruthClassifier enum values
            // c.f. https://gitlab.cern.ch/atlas/athena/blob/master/PhysicsAnalysis/MCTruthClassifier/MCTruthClassifier/MCTruthClassifierDefs.h#L31
            int truth_type = -1;
            int truth_origin = -1;
            truth_type = xAOD::TruthHelpers::getParticleTruthType(*lep);
            truth_origin = xAOD::TruthHelpers::getParticleTruthOrigin(*lep);
            h_type->Fill(truth_type);
            h_origin->Fill(truth_origin);

            // MCTruthClassifier ParticleType 2 is isolated electron
            // MCTruthClassifier ParticleOrigin 10 is "Top" and 12 is "W"
            if(truth_type == 2 && (truth_origin == 12 || truth_origin == 10))
            {
                n_true_lep++;
            }

        } // lep
        for(const auto & lep : *muons)
        {
            if(lep->pt() < lep_pt_cut * 1e-3) continue;
            if(fabs(lep->eta()) > lep_eta_cut) continue;

            // Retrieve MCTruthClassifier enum values
            // c.f. https://gitlab.cern.ch/atlas/athena/blob/master/PhysicsAnalysis/MCTruthClassifier/MCTruthClassifier/MCTruthClassifierDefs.h#L31
            int truth_type = -1;
            int truth_origin = -1;
            truth_type = xAOD::TruthHelpers::getParticleTruthType(*lep);
            truth_origin = xAOD::TruthHelpers::getParticleTruthOrigin(*lep);
            h_type->Fill(truth_type);
            h_origin->Fill(truth_origin);

            // MCTruthClassifier ParticleType 6 is isolated muon
            // MCTruthClassifier ParticleOrigin 10 is "Top" and 12 is "W"
            if(truth_type == 6 && (truth_origin == 12 || truth_origin == 10))
            {
                n_true_lep++;
            }
        }
        h_ttbar_n_lep->Fill(n_true_lep);

    } // entry
    cout << algo << "    Done looping!" << endl;

    rfile->cd();
    h_type->Write();
    h_origin->Write();
    h_ttbar_n_lep->Write();
    cout << algo << "    Some histograms are stored in the output file: " << rfile->GetName() << endl;

    return 0;
}
