# danny_handles_the_truth

Handle that truth.

# Installation

```bash
mkdir test_area/
cd test_area/
mkdir source/
cd source/
asetup AnalysisBase,21.2.73,here
git clone https://:@gitlab.cern.ch:8443/dantrim/danny_handles_the_truth.git
cd ..
mkdir build/
cd build/
cmake ../source
make -j4
source x86*/setup.sh
```

# Running

Once you have run the steps above:

```bash
check_ttbar <input-file> <n-events>
```

This will produce a file ```check_ttbar.root``` which has three histograms: one with the MCTruthClassifier ParticleOrigin information for each lepton (electron or muon),
another with the MCTruthClassifier ParticleType information for each lepton, and another with the number of "true" leptons in the event (classified as being isolated and coming
from a Top quark or W boson).

Note that this runs off of the bare electron and muon containers. No calibration or overlap/etc is applied to any of the objects. The point is
just to show an example of accessing MCTruthClassifier information.

# MCTruthClassifier

The relevant information can be found [here](https://gitlab.cern.ch/atlas/athena/blob/master/PhysicsAnalysis/MCTruthClassifier/MCTruthClassifier/MCTruthClassifierDefs.h#L29).